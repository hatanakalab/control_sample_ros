#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np


class Field:
    def __init__(self, param):
        self._param = param
        dim = param["dim"]
        mesh_acc = np.array(self._param["mesh_acc"])
        self._mesh_acc = mesh_acc
        self._limit = np.array(self._param["limit"])
        # dimension is inverse to X,Y
        self._grid_shape = list(reversed(mesh_acc))
        self._phi = np.ones(self._grid_shape)
        linspace = [
            np.linspace(
                self._param["limit"][i][0], self._param["limit"][i][1], mesh_acc[i]
            )
            for i in range(dim)
        ]
        self._grid = np.meshgrid(*linspace)
        self._zero_index = [np.where(linspace[i] == 0) for i in range(dim)]
        self._point_dense = self.getGridSpan().prod()

    def setPhi(self, phi):
        self._phi = phi

    def getPhi(self, region=None):
        if region is None:
            ret = self._phi
        else:
            ret = self._phi * region
        return ret

    def getGrid(self, region=None):
        if region is None:
            ret = self._grid
        else:
            ret = self._grid[0] * region, self._grid[1] * region
        return ret

    def getGridSpan(self):
        return (self._limit[:, 1] - self._limit[:, 0]) / self._mesh_acc

    def getLimit(self, axes):
        return self._param["limit"][axes]

    def getPointDense(self):
        return self._point_dense

    def getZeroIndex(self):
        return
