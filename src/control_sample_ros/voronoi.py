#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np


class Voronoi(object):
    def __init__(self, field):
        self._field = field
        self.R = 1000 #if you use r-limited voronoi, change self.R

    def setPos(self, pos):
        self._pos = pos

    def setNeighborPos(self, NeighborPos):
        self._neighbor_pos_list = NeighborPos

    def setPhi(self, phi):
        self._field.setPhi(phi)

    def getRegion(self):
        return self._region

    def getCenter(self):
        return self.cent

    def calcRegion(self):
        x_grid, y_grid = self._field.getGrid()
        point_dense = self._field.getPointDense()
        phi = self._field.getPhi()
        region = (x_grid - self._pos[0]) ** 2 + (
            y_grid - self._pos[1]
        ) ** 2 < self.R ** 2
        # create R+margin circle
        outside = (x_grid - self._pos[0]) ** 2 + (y_grid - self._pos[1]) ** 2 < (
            1.1 * self.R
        ) ** 2

        # calculate distance to each point in R+margin circle
        distance = (x_grid * outside - self._pos[0]) ** 2 + (
            y_grid * outside - self._pos[1]
        ) ** 2

        # eliminate R circle from outside (make donut)
        arc = outside * ~region

        for neighborPos in self._neighbor_pos_list:
            # distance to each point from neighbor
            neighborDistance = (x_grid * region - neighborPos[0]) ** 2 + (
                y_grid * region - neighborPos[1]
            ) ** 2
            # nearer points in R+margin circle
            nearRegion = neighborDistance > distance
            # delete points near to neighbor from my Region
            region = region * nearRegion
            # delete points near to neighbor from arc
            arc = arc * nearRegion

        # X-Y cordinates of my region which is weighted
        weightedX = x_grid * phi * region
        weightedY = y_grid * phi * region

        # calculate mass and cent
        self._region = region
        self.mass = np.sum(phi * region) * point_dense
        self.cent = (
            np.array([weightedX.sum(), weightedY.sum()]) * point_dense / self.mass
        )
