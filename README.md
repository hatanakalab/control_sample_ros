# control_sample_ros
ROS講習用プログラム.  
講義資料は```control_sample_ros/doc```に存在

# Dependency

- ROSワークスペース(http://wiki.ros.org/ja/ROS/Tutorials/InstallingandConfiguringROSEnvironment 参照)
- beginner_tutorials パッケージ（http://wiki.ros.org/ja/ROS/Tutorials/CreatingPackage に従い作成)
- pip install pandas numpy 
- sudo apt-get install ros-kinetic-joy
- sudo apt-get install ros-kinetic-joystick-drivers


# Installation
```
cd ~/catkin_ws/src
git clone https://gitlab.com/hatanakalab/control_sample_ros.git
```


# Usage
- 課題実行時
```
roslaunch control_sample_ros feedback.launch
```

- 課題第○問の模範解答(○に数字を入れる)
```
roslaunch control_sample_ros feedback_kadai○.launch
```





