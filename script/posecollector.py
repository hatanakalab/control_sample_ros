#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
from geometry_msgs.msg import Pose, PoseStamped
from geometry_msgs.msg import PoseArray


class Collector:
    def __init__(self, agentName):
        self.ready = False

        topicName = rospy.get_param("~posestampedTopic")
        preTopicName = rospy.get_param("~preTopicName", "/")
        subTopic = preTopicName + agentName + topicName
        rospy.loginfo("topicName:" + subTopic)
        # subscriber for each agent's region
        rospy.Subscriber(subTopic, Pose, self.poseStampedCallback, queue_size=1)
        # initialze with zeros
        self.pose = Pose()

    def poseStampedCallback(self, msg_data):
        if self.ready == False:
            self.ready = True
        self.pose = msg_data

    def getPose(self):
        return self.pose

    def getReady(self):
        return self.ready


class poseCollector:
    def __init__(self):
        # ROS Initialize
        rospy.init_node("poseCollector", anonymous=True)

        # Number of Agents
        self.agentNum = rospy.get_param("/agent_num", 1)
        agent_base = rospy.get_param("~agent_base")

        self.Collectors = []
        # create [Agent's number] subscriber
        for agentID in range(self.agentNum):
            agentName = agent_base + str(agentID)
            collector = Collector(agentName)
            self.Collectors.append(collector)

        self.pub_allPose = rospy.Publisher("/allPose", PoseArray, queue_size=1)
        # node freq
        self.clock = rospy.get_param("/clock", 10)
        self.rate = rospy.Rate(self.clock)

    def spin(self):

        while not rospy.is_shutdown():
            poselist = []
            ready = True
            for agentID in range(self.agentNum):
                pose = self.Collectors[agentID].getPose()
                poselist.append(pose)
                ready = ready * self.Collectors[agentID].getReady()

            if ready:
                self.pub_allPose.publish(PoseArray(poses=poselist))

            self.rate.sleep()


if __name__ == "__main__":
    try:
        posecollector = poseCollector()
        posecollector.spin()

    except rospy.ROSInterruptException:
        pass
