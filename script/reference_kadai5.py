#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import numpy as np

from geometry_msgs.msg import Pose, PoseArray
from std_msgs.msg import Float32MultiArray,MultiArrayLayout,MultiArrayDimension
from control_sample_ros.field import Field
from control_sample_ros.voronoi import Voronoi


class AgentManager:
    def __init__(self):
        rospy.init_node("reference", anonymous=True)
        self.AGENT_ID = rospy.get_param("agent_id")
        self.AGENT_NUM = rospy.get_param("/agent_num")
        field_param = rospy.get_param("/field")
        self._field = Field(field_param)
        self._voronoi = Voronoi(self._field)
        self._all_pos = np.zeros((self.AGENT_NUM, 3))
        self._neighbor_started = False
        self._pub_center = rospy.Publisher("ref", Pose, queue_size=1)
        # self.pub_region = rospy.Publisher('surf_value', Float32MultiArray, queue_size=1)
        # self._color_span = rospy.get_param("field_color")#= np.linspace(0,1,self.AGENT_NUM)

        rospy.Subscriber("/allPose", PoseArray, self.mainCallback)

    def setAllPose(self, msg):
        self._neighbor_started = True
        for i in range(len(msg.poses)):
            pos = [
                msg.poses[i].position.x,
                msg.poses[i].position.y,
                msg.poses[i].position.z,
            ]
            self._all_pos[i] = pos

    def mainCallback(self, msg):
        print(msg)
        if len(msg.poses) == self.AGENT_NUM:
            self.setAllPose(msg)
            center = self.calcVoronoiRegion()
            self.pubCenter(center)          

    def calcVoronoiRegion(self):
        # extract x,y position from list of x,y,z position
        i = self.AGENT_ID
        allPos2d = self._all_pos[:,0:2]
        print(allPos2d)
        pos = allPos2d[i]
        # delete THIS agent position
        neighborPosOnly = np.delete(allPos2d,i , axis=0)
        # set my x,y position, and neighbor's position
        self._voronoi.setPos(pos)
        self._voronoi.setNeighborPos(neighborPosOnly)
        # set information density
        self._voronoi.calcRegion()
        center = self._voronoi.getCenter()
        return center
        # self._region = self._voronoi.getRegion() * self._color_span

    def pubCenter(self, center):
        # for i  in range(self.AGENT_NUM):
        cent_msg = Pose()
        cent_msg.position.x = center[0]
        cent_msg.position.y = center[1]
        self._pub_center.publish(cent_msg)

    # def publishRegion(self):
    #     # publish my sensing region
    #     # make multiarraydimension
    #     region = self._region
    #     dim_ = []
    #     dim_.append(MultiArrayDimension(label="y",size=region.shape[0],stride=region.shape[0]*region.shape[1]))
    #     dim_.append(MultiArrayDimension(label="x",size=region.shape[1],stride=region.shape[1]))
    #     layout_ = MultiArrayLayout(dim=dim_)
    #     region_vec = np.reshape(region,(1,-1)).astype(np.float32).squeeze()
    #     region_for_pub = Float32MultiArray(data=region_vec.tolist(),layout=layout_)
    #     # publish
    #     self.pub_region.publish(region_for_pub) 

if __name__ == "__main__":
    agent = AgentManager()
    rospy.spin()
