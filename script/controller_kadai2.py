#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist, Pose, PoseStamped, Point

class Controller(object):
	def __init__(self):
		rospy.init_node('controller',anonymous=True)
		# gain for P controller
		self.kp = rospy.get_param('gain', default=1)

		rospy.Subscriber('pose',Pose,self.poseCallback)
		rospy.Subscriber('ref',Pose,self.refCallback)# report1(9/18)
		self.velPub = rospy.Publisher('cmd_vel',Twist,queue_size=1)
		self.rate = rospy.Rate(10)

		self.position = Point()
		self.refpos = Point() # report1 9/18

		
	def poseCallback(self,msg):
		self.position = msg.position
		return 0

# report1 9/18 definision
	def refCallback(self,msg):
		self.refpos = msg.position

	def publishVel(self,vx,vy):
		vel_msg = Twist()
		vel_msg.linear.x = vx
		vel_msg.linear.y = vy
		self.velPub.publish(vel_msg)

	def calcVelinput(self):
		# P controller
		vx = self.kp * (self.refpos.x - self.position.x)
		vy = self.kp * (self.refpos.y - self.position.y)
		return vx, vy
		
	def spin(self):
		rospy.sleep(2.)	
		rospy.loginfo("START")
		while not rospy.is_shutdown():
#			vx, vy = self.calcVelinput()
			vx, vy = self.calcVelinput() # report1(9/18)
			self.publishVel(vx, vy)
			self.rate.sleep()
		return 0

if __name__ == '__main__':
	try:
		controller = Controller()	
		controller.spin()
	except rospy.ROSInterruptException: pass
