#!/usr/bin/env python
# license removed for brevity
import rospy
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Joy


ref_p = Pose()

def reference():
    rospy.init_node('reference', anonymous=True)
    pub = rospy.Publisher('ref', Pose, queue_size=10)
    r = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        ref_p.position.x = 1.5 ###
        ref_p.position.y = 0.5 ###
        pub.publish(ref_p)
        r.sleep()

if __name__ == '__main__':
    try:
        reference()
    except rospy.ROSInterruptException: pass
