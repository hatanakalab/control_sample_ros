#!/usr/bin/env python
# license removed for brevity
import rospy
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Joy


ref_p = Pose()
ref_j = Pose() #kadai3

def joycallback(msg):
    ref_j.position.x = - msg.axes[0] 
    ref_j.position.y = msg.axes[1] 

def reference():
    rospy.init_node('reference', anonymous=True)
    pub = rospy.Publisher('ref', Pose, queue_size=10)
    rospy.Subscriber('/joy', Joy, joycallback) #kadai3
    r = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        ref_p.position.x = ref_j.position.x #kadai3
        ref_p.position.y = ref_j.position.y #kadai3
        pub.publish(ref_p)
        r.sleep()

if __name__ == '__main__':
    try:
        reference()
    except rospy.ROSInterruptException: pass
